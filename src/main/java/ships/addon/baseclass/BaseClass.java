package ships.addon.baseclass;

import org.apache.logging.log4j.Logger;

import ships.addon.lib.ProxyClient;
import ships.addon.lib.ProxyCommon;
import ships.addon.lib.Ref;
import ships.addon.blocks_items.TEmast;
import ships.addon.blocks_items.TEsail;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cuchaz.ships.propulsion.PropulsionDiscovererRegistry;

//AlphaOrigins
@Mod(name = Ref.NAME, modid = Ref.MODID, version = Ref.VERSION, dependencies = "required-after:cuchaz.ships")
public class BaseClass {

	@SidedProxy(clientSide = Ref.Client, serverSide = Ref.Common )
	public static ProxyCommon proxy;
	public static ProxyClient cproxy;
	
	public static Logger log = FMLLog.getLogger();
	

	@EventHandler
	public void preInit(FMLInitializationEvent event){
		log.info("Mod Load pre run started");
    	Registry.initBlocks();
    	Registry.initItems();
    	Recipes.initShapedRecipes();
		}
    @EventHandler
    public void init(FMLInitializationEvent event){
    	log.info("Mod Load started");
    	proxy.registerRenders();

    }
    @EventHandler
    public void postInit(FMLInitializationEvent event){
    	log.info("------------------------------------");
    	log.info("    Attempting to Find Ships Mod    ");
    if(Loader.isModLoaded("cuchaz.ships")){
    	PropulsionDiscovererRegistry.addDiscoverer(new TEsailDiscoverer());
    	log.info("          Found Ships Mod           ");
    	log.info("------------------------------------");
    	}
    }

    
    
    @EventHandler
    public void load(FMLInitializationEvent event){
    	proxy.registerRenderInformation();
    }
    public BaseClass(){

    }
    
}
