package ships.addon.baseclass;
//import Sail;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.init.Blocks;
import cuchaz.modsShared.blocks.BlockSet;
import cuchaz.modsShared.blocks.BlockSide;
import cuchaz.modsShared.blocks.BlockUtils;
import cuchaz.modsShared.blocks.BlockUtils.Neighbors;
import cuchaz.modsShared.blocks.Coords;
import cuchaz.ships.BlocksStorage;
import cuchaz.ships.propulsion.PropulsionDiscoverer;
import cuchaz.ships.propulsion.PropulsionMethod;

public class TEsailDiscoverer implements PropulsionDiscoverer {
	
	@Override
	public List<PropulsionMethod> getPropulsionMethods(BlocksStorage shipBlocks, BlockSide frontDirection) {
		// collect all the cloth blocks into connected components
		BlockSet clothCoords = new BlockSet();
		for (Coords coords : shipBlocks.coords()) {
			if (shipBlocks.getBlock(coords).block == Registry.Sail) {
				clothCoords.add(coords);
			}
		}
		List<BlockSet> clothComponents = BlockUtils.getConnectedComponents(clothCoords, Neighbors.Edges);
		
		// build the sails
		List<PropulsionMethod> sails = new ArrayList<PropulsionMethod>();
		for (BlockSet component : clothComponents) {
			Sail sail = new Sail(shipBlocks, component, frontDirection);
			if (sail.isValid()) {
				sails.add(sail);
			}
		}
		return sails;
	}
}
