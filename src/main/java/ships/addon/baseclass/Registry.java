package ships.addon.baseclass;

import ships.addon.blocks_items.BlockMast;
import ships.addon.blocks_items.BlockSail;
import ships.addon.blocks_items.ItemMast;
import ships.addon.blocks_items.ItemSail;
import ships.addon.blocks_items.TestItem;
import ships.addon.blocks_items.TEmast;
import ships.addon.blocks_items.TEsail;
import net.minecraft.item.Item;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;


public class Registry {
	
	public static Item TestItem;
	public static void initItems(){
		TestItem = new TestItem(); GameRegistry.registerItem(TestItem, "test");
	}
	
	public static Block Sail;
	public static Item sail;
	public static Block Mast;
	public static Item mast;
	public static void initBlocks(){
		Sail = new BlockSail(Material.cloth);
		sail = new ItemSail(Sail);
		GameRegistry.registerBlock(Sail,ItemSail.class ,Sail.getUnlocalizedName().substring(5));
		GameRegistry.registerTileEntity(TEsail.class, "sailTile");
		//MinecraftForgeClient.registerItemRenderer(Registry.sail, new ItemSailRenderer());
				
		Mast = new BlockMast(Material.wood);
		mast = new ItemMast(Mast);
		GameRegistry.registerBlock(Mast,ItemMast.class, Mast.getUnlocalizedName().substring(5));
		GameRegistry.registerTileEntity(TEmast.class, "mastTile");
		
		
	}
}
