package ships.addon.baseclass;

import ships.addon.blocks_items.BlockMast;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

public class Recipes {
	public static void initShapedRecipes(){
		for(int i=0; i<6; i++){
		GameRegistry.addShapedRecipe(new ItemStack(Registry.Mast,3,i+6), new Object[]{
			"LLL",
			"WWW",
			'L',new ItemStack(Blocks.planks,1,i),'W',Blocks.wool
		});
		if(i<4){
			GameRegistry.addShapedRecipe(new ItemStack(Registry.Mast,6,i), new Object[]{
				"LLL",
				"WWW",
				'L',new ItemStack(Blocks.log,1,i),'W',Blocks.wool
			});
		}else{
			GameRegistry.addShapedRecipe(new ItemStack(Registry.Mast,6,i), new Object[]{
				"LLL",
				"WWW",
				'L',new ItemStack(Blocks.log2,1,i),'W',Blocks.wool
			});
			}
		}
		//---- SAILS ----\\
		for(int i=0; i<16; i++){
		GameRegistry.addShapedRecipe(new ItemStack(Blocks.wool,4,i), new Object[]{
			"SSS",
			"SSS",
			"SSS",
			'S',new ItemStack(Registry.Sail,1,i)
		});
		GameRegistry.addShapedRecipe(new ItemStack(Registry.Sail,9,i), new Object[]{
			"WW",
			"WW",
			'W',new ItemStack(Blocks.wool,1,i)
		});	
		GameRegistry.addShapedRecipe(new ItemStack(Registry.Sail,3,i), new Object[]{
			"C",
			"C",
			'C',new ItemStack(Blocks.carpet,1,i)
		});
		}
	}	
}
