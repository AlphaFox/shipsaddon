package ships.addon.lib;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ships.addon.blocks_items.BlockSail;
import ships.addon.blocks_items.RenderMast;
import ships.addon.blocks_items.TEmast;
import ships.addon.blocks_items.TEsail;
import ships.addon.blocks_items.RenderSail;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.src.FMLRenderAccessLibrary;

public class ProxyClient  extends ProxyCommon{
	public void registerRenderInformation(){}
	@SideOnly(Side.CLIENT)
	public void registerRenders() {
		//Mast
		TileEntitySpecialRenderer mast = new RenderMast();
		ClientRegistry.bindTileEntitySpecialRenderer(TEmast.class, mast);
		//Sail
		TileEntitySpecialRenderer sail = new RenderSail();
		ClientRegistry.bindTileEntitySpecialRenderer(TEsail.class, sail);	
	}
}
