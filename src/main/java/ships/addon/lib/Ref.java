package ships.addon.lib;

public class Ref {
	public static final String MODID = "SailsAddon";
	public static final String NAME = "SailsAddon";
	public static final String VERSION = "1.0";
	public static final String Client = "ships.addon.lib.ProxyClient";
	public static final String Common = "ships.addon.lib.ProxyCommons";
}
