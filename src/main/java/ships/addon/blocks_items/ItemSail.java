package ships.addon.blocks_items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemSail extends ItemBlock{

	public ItemSail(Block block) {
		super(block);
		this.setHasSubtypes(true);
	}
	public String getUnlocalizedName(ItemStack item){
		int i = item.getItemDamage();
		if(i<0||i>=BlockSail.subBlocks.length){
			i=0;
		}
		return super.getUnlocalizedName()+"."+BlockSail.subBlocks[~i&15];
	}
	public int getMetadata(int meta){
		return meta;
	}
}
