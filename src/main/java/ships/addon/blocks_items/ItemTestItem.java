package ships.addon.blocks_items;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemTestItem extends Item {
	
	public ItemTestItem() {
		this.setUnlocalizedName("test");
		this.setTextureName("SailsAddon"+":"+"test");
		this.setCreativeTab(CreativeTabs.tabTransport);
	} 
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List textList, boolean useAdvancedItemTooltips)
	  {
	    //textList.add("Temporary Test Item");
	    textList.add("Mod created by \u00A7bαlpha");
	    textList.add("youtube: \u00A7cAlpha_Animation");
	    textList.add("IGN: \u00A74AlphaRedstone");
	  }
}
