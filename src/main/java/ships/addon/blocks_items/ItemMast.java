package ships.addon.blocks_items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemMast extends ItemBlock{

	public ItemMast(Block block) {
		super(block);
		this.setHasSubtypes(true);
	}
	public String getUnlocalizedName(ItemStack item){
		int i = item.getItemDamage();
		if(i<0||i>=BlockMast.subBlocks.length){
			i=i-BlockMast.subBlocks.length;
		}
		return super.getUnlocalizedName()+"."+BlockMast.subBlocks[i];
	}
	public int getMetadata(int meta){
		return meta;
	}
}
