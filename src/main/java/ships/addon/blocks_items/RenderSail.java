package ships.addon.blocks_items;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.world.IBlockAccess;
import ships.addon.baseclass.Registry;
import ships.addon.blocks_items.BlockSail;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import ships.addon.lib.Ref;
import ships.addon.models.AcendingSailModel;
import ships.addon.models.MastModel;
import ships.addon.models.SailModel;

public class RenderSail extends TileEntitySpecialRenderer implements ISimpleBlockRenderingHandler{
	

	private static ResourceLocation textureSail = new ResourceLocation("sailsaddon"+":"+"textures/blocks/sail_white.png");
	private SailModel sail;
	private AcendingSailModel acending;
    public static final double POSITION_FIX = -0.5D;
    private static TEsail TEsail = new TEsail();
    
	public RenderSail(){
		this.sail = new SailModel();
		this.acending = new AcendingSailModel();
		}
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float f){
	GL11.glPushMatrix();
		TileEntity loc = tileentity.getWorldObj().getTileEntity(tileentity.xCoord, tileentity.yCoord, tileentity.zCoord);
		textureSail = new ResourceLocation("sailsaddon"+":"+"textures/blocks/sail_"+BlockSail.subBlocks[~loc.getBlockMetadata()&15]+".png");
		GL11.glTranslatef((float)x+0.5F,(float)y-0.5F,(float)z+0.5F);
		
		this.bindTexture(textureSail);
		byte Direction = ((TEsail)loc).getValue();
		int dir = 0;
		dir = Direction;
		if(Direction>1&&Direction<=5){dir = Direction-2;}
		else if(Direction>5){dir = Direction-6;}
		GL11.glRotatef((float)dir*90, 0.0F, 1.0F, 0.0F);
		if(Direction>5){GL11.glTranslatef(0.5F,0.0F,0.0F);}
		if(Direction<=1){this.sail.renderModel(0.0625F);}
		else if(Direction>1){this.acending.renderModel(0.0625F);}
	GL11.glPopMatrix();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelId,RenderBlocks renderer) {
		renderTileEntityAt(TEsail, POSITION_FIX, POSITION_FIX, POSITION_FIX, 0.0F);
	}
    @Override
    public int getRenderId() {
        return Registry.Sail.getRenderType();
    }

	@Override
	public boolean shouldRender3DInInventory(int sail) {
		return true;
	}
	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
		return false;
	}
}

