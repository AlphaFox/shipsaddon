package ships.addon.blocks_items;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatComponentText;

public class TEmast extends TileEntity {
	private byte Direction;
	private byte Dye;
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord + 1, yCoord + 1, zCoord + 1);
	}
	//---- Setters and Getters ----\\
	public byte getValue() {
	    return Direction;
	}
	public void setValue(byte value) {
	    Direction = value;
	}
	public byte getDye() {
	    return Dye;
	}
	public void setDye(byte value) {
		Dye = value;
	}
	//---- Read and Write ----\\
    public void writeToNBT(NBTTagCompound nbt){
    	super.writeToNBT(nbt);
    	nbt.setByte("Direction", this.Direction);
    	nbt.setByte("Dye", this.Dye);
        markForUpdate();
    }
    public void readFromNBT(NBTTagCompound nbt){
    	super.readFromNBT(nbt);
    	this.Direction = nbt.getByte("Direction");
        this.Dye = nbt.getByte("Dye");
    }
    
    //---- Packets ----\\
    @Override
    public Packet getDescriptionPacket(){
    	NBTTagCompound tileTag = new NBTTagCompound();
    	this.writeToNBT(tileTag);
    	return new S35PacketUpdateTileEntity(this.xCoord,this.yCoord,this.zCoord,0,tileTag);   	
    }
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
		this.readFromNBT(pkt.func_148857_g());
	}
	
	//---- Mark for Update ----\\
	public void markForUpdate() {
	    this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}
}
