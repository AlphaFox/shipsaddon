package ships.addon.blocks_items;

import java.util.List;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ships.addon.baseclass.Registry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockMast extends Block implements ITileEntityProvider{
	
	@SideOnly(Side.CLIENT)
	private IIcon[] texture = new IIcon[12];
	final static String[] subBlocks = new String[]{"oak", "spruce", "birch", "jungle", "acacia", "dark"};
	
	public BlockMast(Material material){
		super(material);
		this.setBlockName("mast");
		this.setBlockTextureName("sailsaddon"+":"+(this.getUnlocalizedName().substring(5)));
		this.setCreativeTab(CreativeTabs.tabTransport);
		this.setHardness(0.5F);
	}
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta){
        return texture[meta % subBlocks.length*2]; 
    }
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs ct, List list){
        for (int i = 0; i < subBlocks.length*2; ++i){
        	list.add(new ItemStack(item, 1, i));
        }
    }
	@SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register){
        for (int i = 0; i < subBlocks.length; ++i){
        	texture[i] = register.registerIcon("sailsaddon"+":"+"mast_"+ subBlocks[i]);
        	texture[i+subBlocks.length] = register.registerIcon("sailsaddon"+":"+"mast-"+ subBlocks[i]);
         }
    }
	public void setBlockBoundsForItemRender(){
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}
	public int getRenderType(){
		return -1;
	}
	public boolean isOpaqueCube(){
		return false;
	}
	public int damageDropped(int meta){
		return meta;
	}
	public boolean renderAsNormalBlock(){
		return false;
	}
	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase player, ItemStack itemStack) {
		updateMast(world,x,y,z,(EntityPlayer)player);
	}
	
	//Determine the correct rotation for block
	public void updateMast(World world, int x, int y, int z, EntityPlayer player){
	    if (!world.isRemote && world.getTileEntity(x,y,z) != null && world.getTileEntity(x,y,z) instanceof TEmast) {
	    	byte Direction = 0; int meta=0;
	    	int dir = MathHelper.floor_double((double)((player.rotationYaw * 4F) / 360F) + 0.5D) & 3;
	    	if (Keyboard.isKeyDown(Keyboard.KEY_RSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){dir = dir+1;}
	    	if(dir==0||dir==2||dir==4){Direction = 1; TileEntity blk; 
	    		
				//---- check Below ----\\
	    		blk = loc(world,x,y-1,z-1); // Z - Below
	    		if(blk!=null&&blk instanceof TEsail){Direction=5;meta=blk.getBlockMetadata();
	    		((TEsail) blk).setValue((byte)9);world.markBlockForUpdate(x, y-1, z-1);}
	    		
	    		blk = loc(world,x,y-1,z+1); // Z + Below
	    		if(blk!=null&& blk instanceof TEsail){Direction = 3;meta=blk.getBlockMetadata();
	    		((TEsail)blk).setValue((byte)7);world.markBlockForUpdate(x, y-1, z+1);}
		
	    		//---- check Above ----\\
	    		blk = loc(world,x,y+1,z-1); // Z - Above
	    		if(blk!=null&& blk instanceof TEsail){Direction = 7;meta=blk.getBlockMetadata();
	    		((TEsail)blk).setValue((byte)3);world.markBlockForUpdate(x,y+1,z-1);}
	    		
	    		blk = loc(world,x,y+1,z+1); // Z + Above
	    		if(blk!=null&&blk instanceof TEsail){Direction = 9;meta=blk.getBlockMetadata();
	    		((TEsail)blk).setValue((byte)5);world.markBlockForUpdate(x,y+1,z+1);}
	    		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)!=true){((TEmast)world.getTileEntity(x,y,z)).setDye((byte)meta);}
	    	}
	    	else{Direction = 0; TileEntity blk;
        			//---- check Below ----\\
    			blk = loc(world,x-1,y-1,z); // X - Below
    			if(blk!=null&&blk instanceof TEsail){Direction = 2;meta=blk.getBlockMetadata();
    			((TEsail) blk).setValue((byte)6);world.markBlockForUpdate(x-1, y-1, z);}
    			
    			blk = loc(world,x+1,y-1,z); // X + Below
    			if(blk!=null&& blk instanceof TEsail){Direction = 4;meta=blk.getBlockMetadata();
    			((TEsail)blk).setValue((byte)8);world.markBlockForUpdate(x+1, y-1, z);}
    			
	    		//---- check Above ----\\
	    		blk = loc(world,x-1,y+1,z); // X - Above
	    		if(blk!=null&& blk instanceof TEsail){Direction = 8;meta=blk.getBlockMetadata();
	    		((TEsail)blk).setValue((byte)4);world.markBlockForUpdate(x-1,y+1,z);}
	    		
	    		blk = loc(world,x+1,y+1,z); // X + Above
	    		if(blk!=null&&blk instanceof TEsail){Direction = 6;meta=blk.getBlockMetadata();
	    		((TEsail)blk).setValue((byte)2);world.markBlockForUpdate(x+1,y+1,z);}
	    		
	    		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)!=true){((TEmast)world.getTileEntity(x,y,z)).setDye((byte)meta);}
	        }
	    	TileEntity tileloc = world.getTileEntity(x, y, z);
		        if (tileloc != null && tileloc instanceof TEmast)
		        {
		        	((TEmast)tileloc).setValue(Direction);
		        	world.markBlockForUpdate(x, y, z);
		        }
			int Bound = (int)(Direction/2-Math.round(Direction/2-0.1));		
	    }
	}
	private TileEntity loc(World world,int x, int y, int z) {
		return world.getTileEntity(x, y, z);
	}
    public void setBlockBoundsBasedOnState(IBlockAccess block, int x, int y, int z)
    {
        byte Direction = 0;
		Direction = (byte)((TEmast) block.getTileEntity(x,y,z)).getValue();
        if (Direction==1||Direction==3||Direction==5||Direction==7||Direction==9)
        {
        	this.setBlockBounds(0.0F, 0.25F, 0.25F, 1.0F, 0.75F, 0.75F);
        }
        else
        {
        	this.setBlockBounds(0.25F, 0.25F, 0.0F, 0.75F, 0.75F, 1.0F);
        }
    }
	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TEmast();
	}
	public void breakBlock(World world, int x, int y, int z, int i, int j)
	{
		world.removeTileEntity(x, y, z);
	}
}
