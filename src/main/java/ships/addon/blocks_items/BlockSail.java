package ships.addon.blocks_items;

import java.util.List;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ships.addon.baseclass.Registry;
import ships.addon.models.SailModel;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public class BlockSail extends Block implements ITileEntityProvider{
	
	@SideOnly(Side.CLIENT)
	private IIcon[] texture = new IIcon[16];
	final static String[] subBlocks = new String[]{"black", "red", "green", "brown", "blue", "purple", "cyan", "silver", "gray", "pink", "lime", "yellow", "lightBlue", "magenta", "orange", "white"};
	
	public BlockSail(Material material){
		super(material);
		this.setBlockName("sail");
		this.setCreativeTab(CreativeTabs.tabTransport);
		this.setHardness(0.5F);
		this.setBlockTextureName("sailsaddon"+":"+(this.getUnlocalizedName().substring(5)));
	}
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta){
        return texture[meta % subBlocks.length]; 
    }
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs ct, List list){
        for (int i = 0; i < subBlocks.length; ++i){
        	list.add(new ItemStack(item, 1, i));
        }
    }
	@SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register)
    {
        for (int i = 0; i < subBlocks.length; ++i){
        	texture[i] = register.registerIcon("sailsaddon"+":"+"sail_"+ subBlocks[~i&15]);
         }
    }
	public void setBlockBoundsForItemRender(){
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}
	public int getRenderType(){
		return -1;
	}
	public boolean isOpaqueCube(){
		return false;
	}
	public int damageDropped(int meta){
		return meta;
	}
	public boolean renderAsNormalBlock(){
		return false;
	}
	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase player, ItemStack itemStack) {
		updateSail(world,x,y,z,(EntityPlayer)player);
	}
	//Determine the correct rotation for block
	public void updateSail(World world, int x, int y, int z, EntityPlayer player){
		if (!world.isRemote && world.getTileEntity(x,y,z) != null && world.getTileEntity(x,y,z) instanceof TEsail) {
	    	byte Direction = 0;
	    	int dir = MathHelper.floor_double((double)((player.rotationYaw * 4F) / 360F) + 0.5D) & 3;
	    	if (Keyboard.isKeyDown(Keyboard.KEY_RSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){dir = dir+1;}
	    	if(dir==0||dir==2||dir==4){Direction = 1; TileEntity blk;
	    		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)!=true){
				//---- check Below ----\\
	    		blk = loc(world,x,y-1,z-1); // Z - Below
	    		if(blk!=null&&blk instanceof TEsail){Direction = 5;
	    		((TEsail) blk).setValue((byte)9);world.markBlockForUpdate(x, y-1, z-1);}
	    		else if(blk!=null&& blk instanceof TEmast){Direction = 5;
	    		((TEmast)blk).setValue((byte)9);world.markBlockForUpdate(x, y-1, z-1);}
	    		
	    		blk = loc(world,x,y-1,z+1); // Z + Below
	    		if(blk!=null&& blk instanceof TEsail){Direction = 3;
	    		((TEsail)blk).setValue((byte)7);world.markBlockForUpdate(x, y-1, z+1);}
	    		else if(blk!=null&& blk instanceof TEmast){Direction = 3;
	    		((TEmast)blk).setValue((byte)7);world.markBlockForUpdate(x, y-1, z+1);}
		
	    		//---- check Above ----\\
	    		blk = loc(world,x,y+1,z-1); // Z - Above
	    		if(blk!=null&& blk instanceof TEsail){Direction = 7;
	    		((TEsail)blk).setValue((byte)3);world.markBlockForUpdate(x,y+1,z-1);}
	    		else if(blk!=null&& blk instanceof TEmast){Direction = 7;
	    		((TEmast)blk).setValue((byte)3);world.markBlockForUpdate(x,y+1,z-1);}
	    		
	    		blk = loc(world,x,y+1,z+1); // Z + Above
	    		if(blk!=null&&blk instanceof TEsail){Direction = 9;
	    		((TEsail)blk).setValue((byte)5);world.markBlockForUpdate(x,y+1,z+1);}
	    		else if(blk!=null&&blk instanceof TEmast){Direction = 9;
	    		((TEmast)blk).setValue((byte)5);world.markBlockForUpdate(x,y+1,z+1);}
	    		}
	    	}
	    	else{Direction = 0; TileEntity blk;
        			//---- check Below ----\\
	    		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)!=true){
    			blk = loc(world,x-1,y-1,z); // X - Below
    			if(blk!=null&&blk instanceof TEsail){Direction = 2;
    			((TEsail) blk).setValue((byte)6);world.markBlockForUpdate(x-1, y-1, z);}
    			else if(blk!=null&& blk instanceof TEmast){Direction = 2;
    			((TEmast)blk).setValue((byte)6);world.markBlockForUpdate(x-1, y-1, z);}
    			
    			blk = loc(world,x+1,y-1,z); // X + Below
    			if(blk!=null&& blk instanceof TEsail){Direction = 4;
    			((TEsail)blk).setValue((byte)8);world.markBlockForUpdate(x+1, y-1, z);}
    			else if(blk!=null&& blk instanceof TEmast){Direction = 4;
    			((TEmast)blk).setValue((byte)8);world.markBlockForUpdate(x+1, y-1, z);}
    			
	    		//---- check Above ----\\
	    		blk = loc(world,x-1,y+1,z); // X - Above
	    		if(blk!=null&& blk instanceof TEsail){Direction = 8;
	    		((TEsail)blk).setValue((byte)4);world.markBlockForUpdate(x-1,y+1,z);}
	    		else if(blk!=null&& blk instanceof TEmast){Direction = 8;
	    		((TEmast)blk).setValue((byte)4);world.markBlockForUpdate(x-1,y+1,z);}
	    		
	    		blk = loc(world,x+1,y+1,z); // X + Above
	    		if(blk!=null&&blk instanceof TEsail){Direction = 6;
	    		((TEsail)blk).setValue((byte)2);world.markBlockForUpdate(x+1,y+1,z);}
	    		else if(blk!=null&&blk instanceof TEmast){Direction = 6;
	    		((TEmast)blk).setValue((byte)2);world.markBlockForUpdate(x+1,y+1,z);}
	    		}
	        }
	    	TileEntity tileloc = world.getTileEntity(x, y, z);
		        if (tileloc != null && tileloc instanceof TEsail)
		        {
		        	((TEsail)tileloc).setValue(Direction);
		        	world.markBlockForUpdate(x, y, z);
		        
	    	}
		}
	}
	private TileEntity loc(World world,int x, int y, int z) {
			return world.getTileEntity(x, y, z);
	}
	
	/**----  Block Bounding Box   ----**/
    public void setBlockBoundsBasedOnState(IBlockAccess block, int x, int y, int z)
    {
        byte Direction = 0;
		Direction = (byte)((TEsail) block.getTileEntity(x,y,z)).getValue();
        if (Direction==0){ //---- X X X X
        	this.setBlockBounds(0.4375F, 0.0F, 0.0F, 0.5625F, 1.0F, 1.0F);
        }
		else if (Direction==1){// Z Z Z Z
            this.setBlockBounds(0.0F, 0.0F, 0.4375F, 1.0F, 1.0F, 0.5625F);
        }
        //---- Above ----\\
        else if (Direction==2){ //x-
        	this.setBlockBounds(0.0F, 0.0F, 0.0F, 0.5625F, 1.0F, 1.0F);
        }
        else if (Direction==3){ //z+
        	this.setBlockBounds(0.0F, 0.0F, 0.4375F, 1.0F, 1.0F, 1.0F);
        }
		else if (Direction==4){ //x+
			this.setBlockBounds(0.4375F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        }
		else if (Direction==5){ //z-
			this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.5625F);
        }
        //---- Below ----\\
        else if (Direction==6){ //x+
        	this.setBlockBounds(0.4375F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        }
        else if (Direction==7){ //z-
        	this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.5625F);
        }
		else if (Direction==8){ //x-
			this.setBlockBounds(0.0F, 0.0F, 0.0F, 0.5625F, 1.0F, 1.0F);
        }
		else if (Direction==9){ //z+
			this.setBlockBounds(0.0F, 0.0F, 0.4375F, 1.0F, 1.0F, 1.0F);
        }
		else{
			this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		}     
    }
	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TEsail();
	}	
	public void breakBlock(World world, int x, int y, int z, int i, int j)
	{
		world.removeTileEntity(x, y, z);
	}
}
