package ships.addon.blocks_items;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;

public class TEsail extends TileEntity {
	private byte Direction;
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord + 1, yCoord + 1, zCoord + 1);
	}
	//---- Setters and Getters ----\\
	public byte getValue() {
	    return Direction;
	}
	public void setValue(byte value) {
	    Direction = value;
	}
	
	//---- Read and Write ----\\
    public void writeToNBT(NBTTagCompound nbt)
    {
    	nbt.setByte("Direction", this.Direction);
        super.writeToNBT(nbt);
        markForUpdate();
    }
    public void readFromNBT(NBTTagCompound nbt)
    {
    	this.Direction = nbt.getByte("Direction");
        super.readFromNBT(nbt);
    }
    
    //---- Packets ----\\
    @Override
    public Packet getDescriptionPacket(){
    	NBTTagCompound tileTag = new NBTTagCompound();
    	this.writeToNBT(tileTag);
    	return new S35PacketUpdateTileEntity(this.xCoord,this.yCoord,this.zCoord,0,tileTag);   	
    }
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
		this.readFromNBT(pkt.func_148857_g());
	}
	
	//---- Mark for Update ----\\
	public void markForUpdate() {
	    this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}

}