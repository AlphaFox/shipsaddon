package ships.addon.blocks_items;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import ships.addon.lib.Ref;
import ships.addon.models.MastModel;
import ships.addon.models.MastModelSail;
import ships.addon.models.SailModel;

public class RenderMast extends TileEntitySpecialRenderer{
	
	private static ResourceLocation textureMast = new ResourceLocation("sailsaddon"+":"+"textures/blocks/mast.png");
	private static ResourceLocation textureSail = new ResourceLocation("sailsaddon"+":"+"textures/blocks/sail_white.png");
	private MastModel mast;
	private MastModelSail sail;
	
	public RenderMast(){
		this.mast = new MastModel();
		this.sail = new MastModelSail();
		}

	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float f) {
		TileEntity loc = tileentity.getWorldObj().getTileEntity(tileentity.xCoord, tileentity.yCoord, tileentity.zCoord);
		textureSail = new ResourceLocation("sailsaddon"+":"+"textures/blocks/sail_"+BlockSail.subBlocks[~((TEmast)loc).getDye()&15]+".png");
		if(loc.getBlockMetadata()<BlockMast.subBlocks.length){
			textureMast = new ResourceLocation("sailsaddon"+":"+"textures/blocks/mast_"+BlockMast.subBlocks[loc.getBlockMetadata()]+".png");
		}else{
			textureMast = new ResourceLocation("sailsaddon"+":"+"textures/blocks/mast-"+BlockMast.subBlocks[loc.getBlockMetadata()-BlockMast.subBlocks.length]+".png");
		}
        byte Direction = 0;
		Direction = (byte)((TEmast)loc).getValue();
	GL11.glPushMatrix();
		GL11.glTranslatef((float)x+0.5F,(float)y-0.5F,(float)z+0.5F);
		int dir = 0;
		dir = Direction;
		if(Direction>1&&Direction<=5){dir = Direction-2;}
		else if(Direction>5){dir = Direction-6;}
		GL11.glRotatef((float)dir*90, 0.0F, 1.0F, 0.0F);
		if(Direction<=5&&Direction>1){GL11.glRotatef(90, 0.0F, 0.0F, 1.0F);GL11.glTranslatef(1.0F,-1.0F,0.0F);}
		if(Direction>5){GL11.glRotatef(180, 0.0F, 1.0F, 0.0F);}
		this.bindTexture(textureMast);
		this.mast.renderModel(0.0625F);
		if(Direction>1){
			this.bindTexture(textureSail);
			this.sail.renderModel(0.0625F);
		}
	GL11.glPopMatrix();
	}
}
